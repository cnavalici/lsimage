<?php
error_reporting(E_ERROR);

require_once './src/LSImage.class.php';
require_once './src/LSConfig.php';

echo "start\n";

$ls = new LSImage('EXAMPLE', $config);
$ls->set_image_type('jpg');  # png(default) | jpg | gif
$ls->set_output_type('display');
//$ls->set_output_filename('cmon');
$b = $ls->create();

echo $b;
