<?php
/**
 * CONFIGURATION FILE
 * 
 * @author: Cristian Năvălici ncristian [at] lemonsoftware.eu
 * @package: LSImage 
 */
 
$config = new stdClass;

# IMAGE DETAILS
$config->img_width = 220;
$config->img_height = 80;

# must exists or leave blank for default tmp system location
# no trailing slash
$config->outputdir = '';  

# FONT DETAILS
$config->font = 'font/arial.ttf';
$config->font_size = 24;
$config->font_color = array(240, 230, 0);  # RGB array
$config->font_background = array(20, 60, 0);  # RGB array
