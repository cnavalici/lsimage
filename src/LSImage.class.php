<?php
/**
 * LS IMAGE CLASS
 * 
 * @author: Cristian Năvălici ncristian [at] lemonsoftware.eu
 * @package: LSImage  
 */


class LSImage {
    protected $imgtype;
    protected $outtype;
    protected $outputname;
    protected $outputdir;
    
    private $text_content;
    private $angle = 0;
    private $allowed_imgtypes = array('png', 'jpg', 'jpeg', 'gif');
    
    private $img_types = array(
        'png' => array('func' => 'imagepng'),
        'jpg' => array('func' => 'imagejpeg'),
        'gif' => array('func' => 'imagegif')
    );
    private $default_imgtype = 'png';
    
    private $allowed_outtypes = array('base64', 'file', 'display');


    public function __construct($text = 0, stdClass $config) {
        $this->text_content = $text;
        $this->width = (int)$config->img_width;
        $this->height = (int)$config->img_height;

        $font_size = (int)$config->font_size;
        $this->font_size = ($font_size > $this->height) ? $this->height - 1 : $font_size;
        
        $this->font = $config->font;
        $this->fc = $config->font_color;
        $this->bc = $config->font_background;
        
        $this->outputdir = $config->outputdir ? $config->outputdir : sys_get_temp_dir();
        $this->outputname = $this->set_output_filename();
        
        // default values for required elements
        $this->imgtype = $this->default_imgtype;
        $this->outtype = $this->allowed_outtypes[0];
    }
    
    public function set_image_type($type) {
        $atypes = array_keys($this->img_types);
        $this->imgtype = in_array($type, $atypes) ? $type : $this->default_imgtype;
    }
    
    public function set_output_type($type) {
        $this->outtype = in_array($type, $this->allowed_outtypes) ? $type : $this->allowed_outtypes[0];
    }
    
    public function set_output_filename($filename = '') {
        $this->outputname = trim($filename) ? $filename : uniqid();
    }
    
    public function create() {
        $this->im = imagecreatetruecolor($this->width, $this->height);

        $text_color = imagecolorallocate($this->im, $this->fc[0], $this->fc[1], $this->fc[2]);
        $background_color = imagecolorallocate($this->im, $this->bc[0], $this->bc[1], $this->bc[2]);
        
        imagefilledrectangle($this->im, 0, 0, $this->width, $this->height, $background_color);
        imagettftext(
            $this->im, $this->font_size, $this->angle, 
            $this->calculate_x(), $this->calculate_y(), 
            $text_color, $this->font, $this->text_content);
        
        $image = $this->get_image_factory();

        imagedestroy($this->im);
        
        return $image;
    }
    
    
    private function get_image_factory() {
        switch ($this->outtype) {
            case 'base64': 
                return $this->get_base64();
            break;
            case 'file';
                return $this->get_imagefile();
            break;
            case 'display':
                return $this->get_display();
            break;
        }
    }
    
    private function calculate_x() {
        $bbox = $this->get_bbox();
        return ($this->width + $bbox[0] - $bbox[2]) / 2;
    }
    
    private function calculate_y() {
        $bbox = $this->get_bbox();
        return ($this->height + $bbox[1] - $bbox[7]) / 2;
    }
    
    private function get_bbox() {
        return imageftbbox($this->font_size, $this->angle, $this->font, $this->text_content);
    }
    
    
    private function get_display() {
        header('Content-Type: image/'.$this->imgtype);
        
        $this->output_image();        
    }
    
    private function get_imagefile() {
        $outpath = $this->create_output_path();
        $this->output_image($outpath);
        
        return $outpath;
    }
    
    private function get_base64() {
        @ob_start();
        header('Content-Type: image/'.$this->imgtype);
        
        $this->output_image($outpath);        
        
        $image = ob_get_clean();

        $base64 = 'data:image/'.$this->imgtype.';base64,' . base64_encode($image);

        return $base64;
    }
    
    private function create_output_path() {
        return sprintf("%s/%s.%s", $this->outputdir, $this->outputname, $this->imgtype);
    }
    
    private function output_image($outpath = '') {
        $outfunc = $this->img_types[$this->imgtype]['func'];
        $outfunc($this->im, $outpath);        
    }
}
